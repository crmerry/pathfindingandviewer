// Astar.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "Astar.h"
#include "Heap.h"

/* ***************************************
[Astar]
A*를 이용한 길 찾기 알고리즘.
방문할 노드는 open_list에
방문했던 노드는 close_list에

**************************************** */
/* ***************************************
	매크로
**************************************** */
#define MAP_WIDTH			(200)
#define MAP_HEIGHT			(200)
#define MAP_SCROLL_DISTANCE	(20)

#define TILE_SIZE			(15)
#define DIAGONAL_WEIGHT		(1.5f)
#define SRC_NOT_USED		(-1)
#define	DEST_NOT_USED		(-1)

/* ***************************************
	구조체
**************************************** */
/* ***************************************
	Title map[index_y][index_x]와 같은 방법으로 사용.
	여기서 배열 인덱스가 실질적인 (x,y)이고,
	Tile의 x,y는 화면에 그려줄 좌표.
**************************************** */
struct Tile
{
	enum eType
	{
		Normal = 0, Blocked, ShallowWater, DeepWater
	};

	eType	_type;
	int		_x;
	int		_y;
};

struct Node
{
	float	_G;
	float	_H;
	float	_F;
	int		_x;
	int		_y;
	Node*	_parent;
};

/* ***************************************
	함수
**************************************** */
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK MlessDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);

void Initialize();
void ReInitialize();
void Release();

/* ***************************************
	마우스 클릭 좌표를 보고 어떤 타일인지 검색
	이후 함수들은, 여기서 구한 x, y를 기준으로 진행
**************************************** */
bool SearchTile(int* const index_x, int* const index_y, int mouse_x, int mouse_y);

void FixSrcTile(int index_x, int index_y);
void FixDestTile(int index_x, int index_y);
void FixDragTile(int index_x, int index_y, bool left_button, bool right_button);

void Draw(HDC hdc);
void DrawMap(HDC hdc);
void DrawSrcTile(HDC hdc);
void DrawDestTile(HDC hdc);
void DrawDragTile(HDC hdc);
void DrawSearchingNode(HDC hdc);
void DrawFoundNode(HDC hdc);

/* ***************************************
최소 힙인지, 최대 힙인지 등에 대한
판단 근거는 cmp 함수를 따로 만들어서 제공.
**************************************** */
template <>
bool Heap<Node*>::cmp(Node* node1, Node* node2);

void MakeNode(Node* node, int x, int y, float G, Node* parent);
bool MoveCheck(Node* node);
bool PreCreatedChecker(Node* node);
void Insert(Node* node);
void FindPath();

/* ***************************************
	전역 변수
**************************************** */
HINSTANCE	hInst;                                // 현재 인스턴스입니다.
Tile		g_map[MAP_HEIGHT][MAP_WIDTH]; 
int			g_src_x;
int			g_src_y;
int			g_dest_x;
int			g_dest_y;
int			g_drag_type;

HDC			g_mem_dc;
HBITMAP		g_mem_bitmap;
HBITMAP		g_mem_old_bitmap;

RECT		g_rect;
HWND		g_hWnd;
HWND		g_hMDlg;

std::list<Node*> g_close_list;
Heap<Node*> g_open_list(MAP_WIDTH* MAP_HEIGHT);

bool		g_find_path;
Node*		g_find;
float		g_G_weight = 1.0;
float		g_F_weight = 1.0;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	timeBeginPeriod(1);
	Initialize();
	/* ***************************************
		윈도우 등록
	**************************************** */
	{
		WNDCLASSEXW wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ASTAR));
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_ASTAR);
		wcex.lpszClassName = L"Astar";
		wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		RegisterClassExW(&wcex);
	}

	/* ***************************************
		윈도우 띄우기
	**************************************** */
	{
		hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

		HWND hWnd = CreateWindowW(L"Astar", L"Astar", WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

		if (!hWnd)
		{
			return FALSE;
		}
		g_hWnd = hWnd;

		ShowWindow(hWnd, nCmdShow);
		UpdateWindow(hWnd);
	}

    MSG msg;
	GetClientRect(g_hWnd, &g_rect);

    // 기본 메시지 루프입니다.
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int) msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static bool left_button = false;
	static bool right_button = false;
	static bool drag = false;
	static int map_draw_x = 0;
	static int map_draw_y = 0;

	switch (message)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_LEFT:
			map_draw_x -= MAP_SCROLL_DISTANCE;
			map_draw_x = max(0, map_draw_x);
			break;

		case VK_RIGHT:
			if (map_draw_x + MAP_SCROLL_DISTANCE + g_rect.right < MAP_WIDTH * TILE_SIZE)
			{
				map_draw_x += MAP_SCROLL_DISTANCE;
			}
			break;

		case VK_DOWN:
			if (map_draw_y + MAP_SCROLL_DISTANCE + g_rect.bottom < MAP_HEIGHT * TILE_SIZE)
			{
				map_draw_y += MAP_SCROLL_DISTANCE;
			}
			break;

		case VK_UP:
			map_draw_y -= MAP_SCROLL_DISTANCE;
			map_draw_y = max(0, map_draw_y);
			break;

		case 0x51: // q
			g_G_weight += 0.1f;
			break;

		case 0x57: // w
			g_G_weight -= 0.1f;
			break;

		case 0x45: // e
			g_F_weight += 0.1f;
			break;

		case 0x52: // r
			g_F_weight -= 0.1f;
			break;

		default:
			break;
		}

		InvalidateRect(g_hWnd, NULL, false);
		break;

	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 메뉴 선택을 구문 분석합니다.
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;

	case WM_LBUTTONDOWN:
		left_button = true;
		break;

	case WM_RBUTTONDOWN:
		right_button = true;
		break;

	case WM_MOUSEMOVE:
		if (left_button ||right_button)
		{
			drag = true;

			int x;
			int y;
			if (SearchTile(&x, &y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y))
			{
				FixDragTile(x, y, left_button, right_button);
				InvalidateRect(hWnd, NULL, false);
			}
		}
		break;

	case WM_LBUTTONUP:
		{
			ReInitialize();
			left_button = false;

			if (drag)
			{
				drag = false;
				InvalidateRect(hWnd, NULL, false);

				break;
			}

			int x;
			int y;
			if (SearchTile(&x, &y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y))
			{
				FixSrcTile(x, y);
				InvalidateRect(hWnd, NULL, false);
			}
		}
		break;

	case WM_RBUTTONUP:
		{
			ReInitialize();
			right_button = false;

			if (drag)
			{
				drag = false;
				InvalidateRect(hWnd, NULL, false);

				break;
			}

			int x;
			int y;
			if (SearchTile(&x, &y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y))
			{
				FixDestTile(x, y);
				InvalidateRect(hWnd, NULL, false);
			}
		}
		break;

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);

			if (g_find_path)
			{
				FindPath();
				
				InvalidateRect(hWnd, NULL, false);
			}
			
			Draw(g_mem_dc);
			
			BitBlt(hdc, 0, 0, g_rect.right, g_rect.bottom, g_mem_dc, map_draw_x, map_draw_y, SRCCOPY);

			EndPaint(hWnd, &ps);
        }
        break;

	case WM_SIZE:
		GetClientRect(g_hWnd, &g_rect);
		break;

    case WM_DESTROY:
		SelectObject(g_mem_dc, g_mem_old_bitmap);
		DeleteObject(g_mem_bitmap);
		DeleteDC(g_mem_dc);
		timeEndPeriod(1);
        PostQuitMessage(0);
        break;

	case WM_CREATE:
	{
		HDC hdc = GetDC(hWnd);
		g_mem_dc = CreateCompatibleDC(hdc);
		g_mem_bitmap = CreateCompatibleBitmap(hdc, MAP_WIDTH * TILE_SIZE, MAP_HEIGHT * TILE_SIZE);
		g_mem_old_bitmap = (HBITMAP)SelectObject(g_mem_dc, g_mem_bitmap);
		DeleteDC(hdc);

		if (!IsWindow(g_hMDlg))
		{
			g_hMDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, MlessDlgProc);
			ShowWindow(g_hMDlg, SW_SHOW);
		}
	}
	break;

	default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

INT_PTR CALLBACK MlessDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_INITDIALOG:
	{
		CheckRadioButton(hDlg, IDC_RADIO1, IDC_RADIO3, IDC_RADIO1);
		g_drag_type = Tile::eType::Blocked;
	}
	break;

	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_RADIO1:
			g_drag_type = Tile::eType::Blocked;
			break;

		case IDC_RADIO2:
			g_drag_type = Tile::eType::ShallowWater;
			break;

		case IDC_RADIO3:
			g_drag_type = Tile::eType::DeepWater;
			break;

		case IDC_BUTTON1: // 화면 지우기
			Release();
			InvalidateRect(g_hWnd, NULL, false);
			break;

		case IDC_BUTTON2: // 길 찾기 시작
		{
			ReInitialize();
			g_find_path = true;

			Node* start = new Node;
			MakeNode(start, g_src_x, g_src_y, 0, nullptr);

			g_open_list.push(start);

			InvalidateRect(g_hWnd, NULL, false);
		}
			break;

		case IDC_BUTTON3: // 일시정지
			if (g_find)
			{
				break;
			}

			if (g_find_path)
			{
				SetDlgItemText(hDlg, IDC_BUTTON3, L"재개");
			}
			else
			{
				SetDlgItemText(hDlg, IDC_BUTTON3, L"일시 정지");
			}
			g_find_path = !g_find_path;
			InvalidateRect(g_hWnd, NULL, false);

			break;

		default:
			break;
		}

		break;
	}

	default:
		break;
	}
	return false;
}


INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void Initialize()
{
	g_src_x = SRC_NOT_USED;
	g_src_y = SRC_NOT_USED;
	g_dest_x = DEST_NOT_USED;
	g_dest_y = DEST_NOT_USED;
	
	g_mem_dc = nullptr;
	g_mem_bitmap = nullptr;
	g_mem_old_bitmap = nullptr;
	
	g_find_path = false;
	g_drag_type = Tile::eType::Blocked;
	g_find = nullptr;

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			g_map[y][x]._type = Tile::eType::Normal;
			g_map[y][x]._x = x * TILE_SIZE;
			g_map[y][x]._y = y * TILE_SIZE;
		}
	}
}

void ReInitialize()
{
	g_find_path = false;

	size_t size = g_open_list.size();
	for (size_t index = 0; index < size; index++)
	{
		delete g_open_list[index];
	}

	for (auto e : g_close_list)
	{
		delete e;
	}

	g_open_list.clear();
	g_close_list.clear();
	
	g_find = nullptr;
}

void Release()
{
	ReInitialize();
	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			g_map[y][x]._type = Tile::eType::Normal;
			g_map[y][x]._x = x * TILE_SIZE;
			g_map[y][x]._y = y * TILE_SIZE;
		}
	}
}

bool SearchTile(int* const index_x, int* const index_y, int mouse_x, int mouse_y)
{
	int x = mouse_x / TILE_SIZE;
	int y = mouse_y / TILE_SIZE;
	
	if (x >= 0 && x < MAP_WIDTH && y >= 0 && y < MAP_HEIGHT)
	{
		*index_x = mouse_x / TILE_SIZE;
		*index_y = mouse_y / TILE_SIZE;

		return true;
	}

	return false;
}

void FixSrcTile(int index_x, int index_y)
{
	if (g_src_x == index_x && g_src_y == index_y)
	{
		g_src_x = SRC_NOT_USED;
		g_src_y = SRC_NOT_USED;
		
		return;
	}

	g_src_x = index_x;
	g_src_y = index_y;
}

void FixDestTile(int index_x, int index_y)
{
	if (g_dest_x == index_x && g_dest_y == index_y)
	{
		g_dest_x = DEST_NOT_USED;
		g_dest_y = DEST_NOT_USED;

		return;
	}

	g_dest_x = index_x;
	g_dest_y = index_y;
}

void FixDragTile(int index_x, int index_y, bool left_button, bool right_button)
{
	if (right_button)
	{
		g_map[index_y][index_x]._type = Tile::eType::Normal;
	}
	else if (left_button)
	{
		switch (g_drag_type)
		{
		case Tile::eType::Blocked:
			g_map[index_y][index_x]._type = Tile::eType::Blocked;
			break;

		case Tile::eType::ShallowWater:
			g_map[index_y][index_x]._type = Tile::eType::ShallowWater;
			break;

		case Tile::eType::DeepWater:
			g_map[index_y][index_x]._type = Tile::eType::DeepWater;
			break;

		default:
			assert(0);
			break;
		}
	}
	else
	{

	}
}

void Draw(HDC hdc)
{
	/* ***************************************
		TODO  :Map 출력과 DragTile출력하는 부분은
		하나로 묶을 수도 있을듯.
	**************************************** */
	DrawMap(hdc);
	DrawDragTile(hdc);
	DrawSearchingNode(hdc);
	DrawSrcTile(hdc);
	DrawDestTile(hdc);
	DrawFoundNode(hdc);
}

void DrawMap(HDC hdc)
{
	HBRUSH background = CreateSolidBrush(RGB(215, 215, 215));
	HPEN lattice_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));

	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, background);
	HPEN old_pen = (HPEN)SelectObject(hdc, lattice_line);

	Rectangle(hdc, 0, 0, MAP_WIDTH * TILE_SIZE, MAP_HEIGHT*TILE_SIZE);

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		MoveToEx(hdc, g_map[y][0]._x, g_map[y][0]._y, NULL);
		LineTo(hdc, MAP_WIDTH* TILE_SIZE, g_map[y][0]._y);
	}

	for (int x = 0; x < MAP_WIDTH; x++)
	{
		MoveToEx(hdc, g_map[0][x]._x, g_map[0][x]._y, NULL);
		LineTo(hdc, g_map[0][x]._x, MAP_HEIGHT*TILE_SIZE);
	}

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);

	DeleteObject(background);
	DeleteObject(lattice_line);
}

void DrawSrcTile(HDC hdc)
{
	if (g_src_x == SRC_NOT_USED)
	{
		return;
	}

	HBRUSH slot_background = CreateSolidBrush(RGB(34, 177, 76));
	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, slot_background);
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));
	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);

	int src_x = g_map[g_src_y][g_src_x]._x;
	int src_y = g_map[g_src_y][g_src_x]._y;

	Rectangle(hdc, src_x, src_y, src_x + TILE_SIZE, src_y + TILE_SIZE);

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);
	DeleteObject(out_line);
	DeleteObject(slot_background);
}

void DrawDestTile(HDC hdc)
{
	if (g_dest_x == DEST_NOT_USED)
	{
		return;
	}

	HBRUSH slot_background = CreateSolidBrush(RGB(255, 0, 0));
	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, slot_background);
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));
	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);
	int dest_x = g_map[g_dest_y][g_dest_x]._x;
	int dest_y = g_map[g_dest_y][g_dest_x]._y;

	Rectangle(hdc, dest_x, dest_y, dest_x + TILE_SIZE, dest_y + TILE_SIZE);

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);
	DeleteObject(out_line);
	DeleteObject(slot_background);
}

void DrawDragTile(HDC hdc)
{
	HBRUSH blocked = CreateSolidBrush(RGB(100, 100, 100));
	HBRUSH shallow_water = CreateSolidBrush(RGB(153, 217, 234));
	HBRUSH deep_water = CreateSolidBrush(RGB(0, 162, 232));
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));

	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);
	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, blocked);

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			switch (g_map[y][x]._type)
			{
			case Tile::eType::Blocked:
			{
				SelectObject(hdc, blocked);
				int blocked_x = g_map[y][x]._x;
				int blocked_y = g_map[y][x]._y;

				Rectangle(hdc, blocked_x, blocked_y, blocked_x + TILE_SIZE, blocked_y + TILE_SIZE);
			}
				break;

			case Tile::eType::ShallowWater:
			{
				SelectObject(hdc, shallow_water);
				int blocked_x = g_map[y][x]._x;
				int blocked_y = g_map[y][x]._y;

				Rectangle(hdc, blocked_x, blocked_y, blocked_x + TILE_SIZE, blocked_y + TILE_SIZE);
			}		
				break;

			case Tile::eType::DeepWater:
			{
				SelectObject(hdc, deep_water);
				int blocked_x = g_map[y][x]._x;
				int blocked_y = g_map[y][x]._y;

				Rectangle(hdc, blocked_x, blocked_y, blocked_x + TILE_SIZE, blocked_y + TILE_SIZE);
			}		
				break;

			default:

				break;
			}
		}
	}

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);
	DeleteObject(out_line);
	DeleteObject(blocked);
	DeleteObject(deep_water);
	DeleteObject(shallow_water);
}

void DrawFoundNode(HDC hdc)
{
	if (!g_find)
	{
		return;
	}

	HPEN find_color = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	HPEN old_pen = (HPEN)SelectObject(hdc, find_color);

	Node* find = g_find;
	MoveToEx(hdc, g_map[g_dest_y][g_dest_x]._x + TILE_SIZE / 2, g_map[g_dest_y][g_dest_y]._x + TILE_SIZE / 2, NULL);

	while (g_find)
	{
		int x = g_map[g_find->_y][g_find->_x]._x;
		int y = g_map[g_find->_y][g_find->_x]._y;

		LineTo(hdc, x + TILE_SIZE / 2, y + TILE_SIZE / 2);
		MoveToEx(hdc, x + TILE_SIZE / 2, y + TILE_SIZE / 2, NULL);
		g_find = g_find->_parent;
	}

	g_find = find;

	SelectObject(hdc, old_pen);
	DeleteObject(find_color);
}

void DrawSearchingNode(HDC hdc)
{
	HBRUSH close_color = CreateSolidBrush(RGB(255, 201, 14));
	HBRUSH open_color = CreateSolidBrush(RGB(86, 83, 164));

	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));

	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, close_color);
	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);

	for (Node* node : g_close_list)
	{
		int x = g_map[node->_y][node->_x]._x;
		int y = g_map[node->_y][node->_x]._y;

		Rectangle(hdc, x, y, x + TILE_SIZE, y + TILE_SIZE);
	}

	SelectObject(hdc, open_color);

	size_t size = g_open_list.size();
	for (size_t index = 0; index < size; index++)
	{
		Node* node = g_open_list[index];

		int x = g_map[node->_y][node->_x]._x;
		int y = g_map[node->_y][node->_x]._y;

		Rectangle(hdc, x, y, x + TILE_SIZE, y + TILE_SIZE);
	}

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);

	DeleteObject(out_line);
	DeleteObject(open_color);
	DeleteObject(close_color);
}

/* ***************************************
	최소 힙인지, 최대 힙인지 등에 대한
	판단 근거는 cmp 함수를 따로 만들어서 제공.
**************************************** */
template <>
bool Heap<Node*>::cmp(Node* node1, Node* node2)
{
	return node1->_F < node2->_F;
}


void MakeNode(Node* node, int x, int y, float G, Node* parent)
{
	float tile_weight = 1.0f;

	switch (g_map[y][x]._type)
	{
	case Tile::eType::DeepWater:
		tile_weight = 1.7f;
		break;
	case Tile::eType::ShallowWater:
		tile_weight = 1.2f;
		break;

	default:

		break;
	}

	node->_x = x;
	node->_y = y;
	node->_G = G * g_G_weight;
	node->_H = (float)(abs(node->_x - g_dest_x) + abs(node->_y - g_dest_y));
	node->_F = (float)((node->_G + node->_H) * g_F_weight * tile_weight);
	node->_parent = parent;
}

bool MoveCheck(Node* node)
{
	int x = node->_x;
	int y = node->_y;

	if (x >= 0 && x < MAP_WIDTH && y >= 0 && y < MAP_HEIGHT)
	{
		if (g_map[y][x]._type != Tile::eType::Blocked)
		{
			return true;
		}
	}

	return false;
}

bool PreCreatedChecker(Node* node)
{
	int size = (int)g_open_list.size();

	for (int index = 0; index < size; index++)
	{
		if (node->_x == g_open_list[index]->_x && node->_y == g_open_list[index]->_y)
		{
			if (g_open_list[index]->_G > node->_G)
			{
				float dif = g_open_list[index]->_G - node->_G;
				g_open_list[index]->_parent = node->_parent;
				g_open_list[index]->_G = node->_G;
				g_open_list[index]->_F -= dif;
			}

			return false;
		}
	}

	for (Node* n : g_close_list)
	{
		if (n->_x == node->_x && n->_y == node->_y)
		{
			if (n->_G > node->_G)
			{
				float dif = n->_G - node->_G;
				n->_parent = node->_parent;
				n->_G = node->_G;
			}

			return false;
		}
	}

	return true;
}

void Insert(Node* node)
{
	if (MoveCheck(node))
	{
		if (PreCreatedChecker(node))
		{
			g_open_list.push(node);

			return;
		}
	}

	delete node;
}

void FindPath()
{
	if (g_open_list.empty())
	{
		g_find = nullptr;
		g_find_path = false;

		return;
	}

	Node* node = g_open_list.peek();
	g_open_list.pop();
	g_close_list.push_back(node);

	if (node->_x == g_dest_x && node->_y == g_dest_y)
	{
		g_find = node;
		g_find_path = false;

		return;
	}

	Node* RR = new Node;
	Node* RD = new Node;
	Node* DD = new Node;
	Node* LD = new Node;
	Node* LL = new Node;
	Node* LU = new Node;
	Node* UU = new Node;
	Node* RU = new Node;

	MakeNode(RR, node->_x + 1, node->_y, node->_G + 1.0f, node);
	MakeNode(RD, node->_x + 1, node->_y + 1, node->_G + DIAGONAL_WEIGHT, node);
	MakeNode(DD, node->_x, node->_y + 1, node->_G + 1.0f, node);
	MakeNode(LD, node->_x - 1, node->_y + 1, node->_G + DIAGONAL_WEIGHT, node);
	MakeNode(LL, node->_x - 1, node->_y, node->_G + 1.0f, node);
	MakeNode(LU, node->_x - 1, node->_y - 1, node->_G + DIAGONAL_WEIGHT, node);
	MakeNode(UU, node->_x, node->_y - 1, node->_G + 1.0f, node);
	MakeNode(RU, node->_x + 1, node->_y - 1, node->_G + DIAGONAL_WEIGHT, node);

	Insert(RR);
	Insert(RD);
	Insert(DD);
	Insert(LD);
	Insert(LL);
	Insert(LU);
	Insert(UU);
	Insert(RU);
}