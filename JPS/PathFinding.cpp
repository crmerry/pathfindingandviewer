#include "stdafx.h"
#include <math.h>
#include <vector>
#include <list>
#include <algorithm>
#include "PathFinding.h"

using namespace pathfinding;

/* ***************************************
	Node*에 대한 비교 함수를 템플릿 특수화를
	통하여 제공해줌.
	클래스의 포인터는 일반 자료형의 포인터와
	마찬가지이기 때문에 대소 비교를 하면
	주소값의 대소 비교가 되기 때문.
**************************************** */

bool pathfinding::CompareNode(Node* node1, Node* node2)
{
	return node1->_F > node2->_F;
}

BasePathFinding::BasePathFinding(float G_weight, float H_weight, int open_list_size, TileChecker tile_check_function)
	:_G_weight(G_weight), _H_weight(H_weight), TileCheck(tile_check_function)
{
	_open_list_size = open_list_size;
	_open_list = new std::vector<Node*>;
	_close_list = new std::list<Node*>;
	_find = nullptr;
}

BasePathFinding::~BasePathFinding()
{
	for (auto e : *_open_list)
	{
		delete e;
	}

	for (auto e : *_close_list)
	{
		delete e;
	}

	delete _open_list;
	delete _close_list;
}

Node* pathfinding::BasePathFinding::MakeNode(int x, int y, int dest_x, int dest_y, float G, Node* parent)
{
	Node* node = new Node;

	node->_pos._x = x;
	node->_pos._y = y;
	node->_G = G * _G_weight;
	node->_H = (float)((float)abs(node->_pos._x - dest_x) + (float)abs(node->_pos._y - dest_y))*_H_weight;
	node->_F = (float)((node->_G + node->_H));
	node->_parent = parent;

	return node;
}

bool BasePathFinding::MoveCheck(Node* node)
{
	return TileCheck(node->_pos._x, node->_pos._y);
}

bool BasePathFinding::PreCreatedChecker(Node* node)
{
	int size = (int)_open_list->size();
	int x = node->_pos._x;
	int y = node->_pos._y;

	for (Node* n : *_open_list)
	{
		if (x == n->_pos._x && y == n->_pos._y)
		{
			return false;
		}
	}

	for (Node* n : *_close_list)
	{
		if (x == n->_pos._x && y == n->_pos._y)
		{
			return false;
		}
	}

	return true;
}

bool BasePathFinding::Insert(Node* node)
{
	if (_open_list->size() >= _open_list_size)
	{
		delete node;

		return false;
	}

	if (MoveCheck(node) && PreCreatedChecker(node))
	{
		_open_list->push_back(node);
		
		return true;
	}

	delete node;

	return false;
}

bool BasePathFinding::ComparePosWithTile(int x, int y)
{
	return false;
}

bool BasePathFinding::ComparePosWithDest(int x, int y, int dest_x, int dest_y)
{
	return (x == dest_x && y == dest_y);
}

void pathfinding::BasePathFinding::FixLineByBresenham()
{
	if (!_find)
	{
		return;
	}

	std::vector<Pos> trace;
	Node* find = _find;
	Node* parent = find->_parent;

	while (find && parent)
	{
		int dest_x = parent->_pos._x;	
		int dest_y = parent->_pos._y;
		int src_x = find->_pos._x;
		int src_y = find->_pos._y;

		int dif_x = dest_x - src_x;
		int dif_y = dest_y - src_y;
		int abs_dif_x = abs(dif_x);
		int abs_dif_y = abs(dif_y);

		int direct_x = 0;
		int direct_y = 0;

		if (abs_dif_x != 0)
		{
			direct_x = dif_x / abs_dif_x;
		}

		if (abs_dif_y != 0)
		{
			direct_y = dif_y / abs_dif_y;
		}

		/* ***************************************
			trace에 직선에 대한 좌표가 누적됨.
		**************************************** */
		if (abs_dif_x > abs_dif_y)
		{
			int error = abs_dif_x / 2;

			trace.push_back(Pos{ src_x, src_y });

			for (int index = 0; index < abs_dif_x; index++)
			{
				error += abs_dif_y;

				if (error >= abs_dif_x)
				{
					src_y += direct_y;
					error -= abs_dif_x;
				}

				src_x += direct_x;
				trace.push_back(Pos{ src_x, src_y });
			}
		}
		else
		{
			int error = abs_dif_y / 2;

			trace.push_back(Pos{ src_x, src_y });
			for (int index = 0; index < abs_dif_y; index++)
			{
				error += abs_dif_x;

				if (error >= abs_dif_y)
				{
					src_x += direct_x;
					error -= abs_dif_y;
				}

				src_y += direct_y;
				trace.push_back(Pos{ src_x, src_y });
			}
		}

		/* ***************************************
			trace에 적힌 좌표들 중에 막힌 곳이 있는지
			타일 체크.
		**************************************** */
		size_t size = trace.size();
		bool check = true;

		for (size_t index = 0; index < size; index++)
		{
			if (!TileCheck(trace[index]._x, trace[index]._y))
			{
				check = false;

				break;
			}
		}

		/* ***************************************
		막힌 곳이 없으면, 현재 find는 그대로 두고, 
		find의 parent의 parent로 가면서 조사.
		**************************************** */
		if (check)
		{
			find->_parent = parent;
			parent = parent->_parent;
		}
		else
		{
			/* ***************************************
			막힌 곳이 발견되면, parent부터 다시 조사.
			**************************************** */
			find = parent;
			parent = parent->_parent;
		}
	}

	/* ***************************************
	계산 완료되면, 
	find -> n1 -> n2 -> n3 -> n4 .... n_src 이던 _find의 연결고리가

	find -> n3 -> n5 ...->n_src로 변함.

	이때, 연겨링 끊긴 n2와 n4 등은 어떻게 처리 하느냐?
	**************************************** */
}

JPS::JPS(float G_weight, float H_weight, int open_list_size, TileChecker tile_check_function)
	:BasePathFinding(G_weight, H_weight, open_list_size, tile_check_function)
{
}

JPS::~JPS()
{
}

bool JPS::GetNextPath(int* next_x, int* next_y, int start_x, int start_y, int dest_x, int dest_y)
{
	if (FindPath(start_x, start_y, dest_x, dest_y))
	{
		FixLineByBresenham();

		Node* prev = _find;

		while (_find->_parent)
		{
			prev = _find;
			_find = _find->_parent;
		}

		*next_x = prev->_pos._x;
		*next_y = prev->_pos._y;

		return true;
	}
	else
	{
		return false;
	}
}

bool JPS::GetAllPath(Pos** path, int start_x, int start_y, int dest_x, int dest_y)
{
	if (FindPath(start_x, start_y, dest_x, dest_y))
	{
		FixLineByBresenham();

		Node* prev = _find;
		Node* find = _find;
		int count = 0;

		while (find->_parent)
		{
			prev = find;
			find = find->_parent;
			count++;
		}

		if (count == 0)
		{
			*path = new Pos[1];
			(*path)[0]._x = prev->_pos._x;
			(*path)[0]._y = prev->_pos._y;

			return true;
		}

		*path = new Pos[count];

		find = _find;

		while (find->_parent)
		{
			(*path)[--count]._x = find->_pos._x;
			(*path)[count]._y = find->_pos._y;
			find = find->_parent;
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool JPS::SearchRR(int x, int y, int dest_x, int dest_y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!TileCheck(x,y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x,	dest_y, (float)abs(parent->_pos._x- (x)) + (float)abs(parent->_pos._y - (y)), parent);
			if (Insert(node))
			{
				_find = node;
			}
		}

		return true;
	}

	bool b_right_up = !TileCheck(x, y - 1) && TileCheck(x + 1, y - 1);
	bool b_right_down = !TileCheck(x, y + 1) && TileCheck(x + 1, y + 1);
		
	if (b_right_up || b_right_down)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_right_up)
				{
					JumpRU(x + 1, y - 1, dest_x, dest_y, node_parent);
				}

				if (b_right_down)
				{
					JumpRD(x + 1, y + 1, dest_x, dest_y, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchRR(x + 1, y, dest_x, dest_y, parent, b_only_horizontal_vertical, false);
}

bool JPS::SearchDD(int x, int y, int dest_x, int dest_y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!TileCheck(x, y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
			if (Insert(node))
			{
				_find = node;
			}
		}

		return true;
	}

	bool b_left_down = !TileCheck(x - 1, y) && TileCheck(x - 1, y + 1);
	bool b_right__down = !TileCheck(x + 1, y) && TileCheck(x + 1, y + 1);
		
	if (b_left_down || b_right__down)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_left_down)
				{
					JumpLD(x - 1, y + 1, dest_x, dest_y, node_parent);
				}

				if (b_right__down)
				{
					JumpRD(x + 1, y + 1, dest_x, dest_y, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchDD(x, y + 1, dest_x, dest_y, parent, b_only_horizontal_vertical, false);
}

bool JPS::SearchLL(int x, int y, int dest_x, int dest_y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!TileCheck(x, y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
			if (Insert(node))
			{
				_find = node;
			}
		}

		return true;
	}

	bool b_left_up = !TileCheck(x, y - 1) && TileCheck(x - 1, y - 1);
	bool b_left_down = !TileCheck(x, y + 1) && TileCheck(x - 1, y + 1);

	if (b_left_down || b_left_up)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_left_up)
				{
					JumpLU(x - 1, y - 1, dest_x, dest_y, node_parent);
				}

				if (b_left_down)
				{
					JumpLD(x - 1, y + 1, dest_x, dest_y, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchLL(x - 1, y, dest_x, dest_y, parent, b_only_horizontal_vertical, false);
}

bool JPS::SearchUU(int x, int y, int dest_x, int dest_y, Node * parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!TileCheck(x, y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
			if (Insert(node))
			{
				_find = node;
			}
			
		}

		return true;
	}

	bool b_left_up = !TileCheck(x - 1, y) && TileCheck(x - 1, y - 1);
	bool b_right_up = !TileCheck(x + 1, y) && TileCheck(x + 1, y - 1);
		
	if (b_left_up || b_right_up)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_left_up)
				{
					JumpLU(x - 1, y - 1, dest_x, dest_y, node_parent);
				}

				if (b_right_up)
				{
					JumpRU(x + 1, y - 1, dest_x, dest_y, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchUU(x, y - 1, dest_x, dest_y, parent, b_only_horizontal_vertical, false);
}

void JPS::SearchRU(int x, int y, int dest_x, int dest_y, Node* parent)
{
	SearchRR(x, y, dest_x, dest_y, parent, true, true);
	SearchUU(x, y, dest_x, dest_y, parent, true, true);

	JumpRU(x + 1, y - 1, dest_x, dest_y, parent);
}

void JPS::SearchRD(int x, int y, int dest_x, int dest_y, Node* parent)
{
	SearchRR(x, y, dest_x, dest_y, parent, true, true);
	SearchDD(x, y, dest_x, dest_y, parent, true, true);

	JumpRD(x + 1, y + 1, dest_x, dest_y, parent);
}

void JPS::SearchLU(int x, int y, int dest_x, int dest_y, Node* parent)
{
	SearchLL(x, y, dest_x, dest_y, parent, true, true);
	SearchUU(x, y, dest_x, dest_y, parent, true, true);

	JumpLU(x - 1, y - 1, dest_x, dest_y, parent);
}

void JPS::SearchLD(int x, int y, int dest_x, int dest_y, Node* parent)
{
	SearchLL(x, y, dest_x, dest_y, parent, true, true);
	SearchDD(x, y, dest_x, dest_y, parent, true, true);

	JumpLD(x - 1, y + 1, dest_x, dest_y, parent);
}

void JPS::JumpRU(int x, int y, int dest_x, int dest_y, Node* parent)
{
	if (!TileCheck(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		if (Insert(node))
		{
			_find = node;
		}
		
		return;
	}
	
	if ((!TileCheck(x - 1, y) && TileCheck(x - 1, y - 1)) || (!TileCheck(x, y + 1) && TileCheck(x + 1, y + 1)))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchRR(x + 1, y, dest_x, dest_y, parent, false, false) || SearchUU(x, y - 1, dest_x, dest_y, parent, false, false))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	JumpRU(x + 1, y - 1, dest_x, dest_y, parent);
}

void JPS::JumpRD(int x, int y, int dest_x, int dest_y, Node* parent)
{
	if (!TileCheck(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		if (Insert(node))
		{
			_find = node;
		}

		return;
	}
		
	if ((!TileCheck(x, y - 1) && TileCheck(x + 1, y - 1)) || (!TileCheck(x - 1, y) && TileCheck(x - 1, y + 1)))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchRR(x + 1, y, dest_x, dest_y, parent, false, false) || SearchDD(x, y + 1, dest_x, dest_y, parent, false, false))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y,  (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	JumpRD(x + 1, y + 1, dest_x, dest_y, parent);
}

void JPS::JumpLU(int x, int y, int dest_x, int dest_y, Node* parent)
{
	if (!TileCheck(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		if (Insert(node))
		{
			_find = node;
		}

		return;
	}

	if ((!TileCheck(x + 1, y) && TileCheck(x + 1, y - 1)) || (!TileCheck(x, y + 1) && TileCheck(x - 1, y + 1)))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchLL(x - 1, y, dest_x, dest_y, parent, false, false) || SearchUU(x, y - 1, dest_x, dest_y, parent, false, false))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	JumpLU(x - 1, y - 1, dest_x, dest_y, parent);
}

void JPS::JumpLD(int x, int y, int dest_x, int dest_y, Node* parent)
{
	if (!TileCheck(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y, dest_x, dest_y))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		if (Insert(node))
		{
			_find = node;
		}

		return;
	}

	if ((!TileCheck(x, y - 1) && TileCheck(x - 1, y - 1)) || (!TileCheck(x + 1, y) && TileCheck(x + 1, y + 1)))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchLL(x - 1, y, dest_x, dest_y, parent, false, false) || SearchDD(x, y + 1, dest_x, dest_y, parent, false, false))
	{
		Node* node = MakeNode(x, y, dest_x, dest_y, (float)abs(parent->_pos._x - (x)) + (float)abs(parent->_pos._y - (y)), parent);
		Insert(node);

		return;
	}

	JumpLD(x - 1, y + 1, dest_x, dest_y, parent);
}

bool JPS::FindPath(int start_x, int start_y, int dest_x, int dest_y)
{
	_open_list->clear();
	_close_list->clear();

	_find = false;

	Node* start = MakeNode(start_x, start_y, dest_x, dest_y, 0, nullptr);
	_open_list->push_back(start);

	while (1)
	{
		if (_find)
		{
			return true;
		}

		if (_open_list->empty() || _open_list->size() >= _open_list_size)
		{
			return false;
		}

		Node* node = _open_list->back();
		_open_list->pop_back();
		_close_list->push_back(node);

		int x = node->_pos._x;
		int y = node->_pos._y;
	
		if (node->_parent)
		{
			int parent_x = node->_parent->_pos._x;
			int parent_y = node->_parent->_pos._y;

			int left_or_right = x - parent_x;
			int up_or_down = y - parent_y;

			if (left_or_right > 0)
			{
				if (up_or_down < 0)
				{
					SearchRU(x, y, dest_x, dest_y, node);
				}
				else if (up_or_down == 0)
				{
					SearchRR(x, y, dest_x, dest_y, node, true, true);
				}
				else
				{
					SearchRD(x, y, dest_x, dest_y, node);
				}
			}
			else if (left_or_right == 0)
			{
				if (up_or_down < 0)
				{
					SearchUU(x, y, dest_x, dest_y, node, true, true);
				}
				else
				{
					SearchDD(x, y, dest_x, dest_y, node, true, true);
				}
			}
			else
			{
				if (up_or_down < 0)
				{
					SearchLU(x, y, dest_x, dest_y, node);
				}
				else if (up_or_down == 0)
				{
					SearchLL(x, y, dest_x, dest_y, node, true, true);
				}
				else
				{
					SearchLD(x, y, dest_x, dest_y, node);
				}
			}
		}
		else
		{
			SearchUU(x, y, dest_x, dest_y, node, true, true);
			SearchRR(x, y, dest_x, dest_y, node, true, true);
			SearchDD(x, y, dest_x, dest_y, node, true, true);
			SearchLL(x, y, dest_x, dest_y, node, true, true);
			SearchLU(x, y, dest_x, dest_y, node);
			SearchRU(x, y, dest_x, dest_y, node);
			SearchRD(x, y, dest_x, dest_y, node);
			SearchLD(x, y, dest_x, dest_y, node);
		}

		std::sort(_open_list->begin(), _open_list->end(), CompareNode);
	}
}

pathfinding::AStar::AStar(float G_weight, float H_weight, int open_list_size, TileChecker tile_check_function)
	:BasePathFinding(G_weight, H_weight, open_list_size, tile_check_function)
{
}

pathfinding::AStar::~AStar()
{
}

bool pathfinding::AStar::GetNextPath(int * next_x, int * next_y, int start_x, int start_y, int dest_x, int dest_y)
{
	if (FindPath(start_x, start_y, dest_x, dest_y))
	{
		FixLineByBresenham();

		Node* prev = _find;

		while (_find->_parent)
		{
			prev = _find;
			_find = _find->_parent;
		}

		*next_x = prev->_pos._x;
		*next_y = prev->_pos._y;

		return true;
	}
	else
	{
		return false;
	}
}

bool pathfinding::AStar::GetAllPath(Pos ** path, int start_x, int start_y, int dest_x, int dest_y)
{
	if (FindPath(start_x, start_y, dest_x, dest_y))
	{
		FixLineByBresenham();

		Node* prev = _find;
		Node* find = _find;
		int count = 0;

		while (find->_parent)
		{
			prev = find;
			find = find->_parent;
			count++;
		}

		if (count == 0)
		{
			*path = new Pos[1];
			(*path)[0]._x = prev->_pos._x;
			(*path)[0]._y = prev->_pos._y;

			return true;
		}

		*path = new Pos[count];

		find = _find;

		while (find->_parent)
		{
			(*path)[--count]._x = find->_pos._x;
			(*path)[count]._y = find->_pos._y;
			find = find->_parent;
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool pathfinding::AStar::FindPath(int start_x, int start_y, int dest_x, int dest_y)
{
	_open_list->clear();
	_close_list->clear();

	_find = false;

	Node* start = MakeNode(start_x, start_y, dest_x, dest_y, 0, nullptr);
	Insert(start);
	
	while (1)
	{
		if (_open_list->empty() || _open_list->size() >= _open_list_size)
		{
			return false;
		}

		Node* node = _open_list->back();
		_open_list->pop_back();
		_close_list->push_back(node);

		int x = node->_pos._x;
		int y = node->_pos._y;
		float G = node->_G;
		
		if (ComparePosWithDest(x, y, dest_x, dest_y))
		{
			_find = node;

			return true;
		}

		Node* RR = MakeNode(x + 1, y, dest_x, dest_y, node->_G + 1.0f, node);;
		Node* RD = MakeNode(x + 1, y + 1, dest_x, dest_y, node->_G + 1.6f, node);
		Node* DD = MakeNode(x, y + 1, dest_x, dest_y, node->_G + 1.0f, node);
		Node* LD = MakeNode(x - 1, y + 1, dest_x, dest_y, node->_G + 1.6f, node);
		Node* LL = MakeNode(x - 1, y, dest_x, dest_y, node->_G + 1.0f, node);
		Node* LU = MakeNode(x - 1, y - 1, dest_x, dest_y, node->_G + 1.6f, node);
		Node* UU = MakeNode(x, y - 1, dest_x, dest_y, node->_G + 1.0f, node);
		Node* RU = MakeNode(x + 1, y - 1, dest_x, dest_y, node->_G + 1.6f, node);

		Insert(RR);
		Insert(RD);
		Insert(DD);
		Insert(LD);
		Insert(LL);
		Insert(LU);
		Insert(UU);
		Insert(RU);

		std::sort(_open_list->begin(), _open_list->end(), CompareNode);
	}
}
