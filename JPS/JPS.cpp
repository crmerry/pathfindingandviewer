// PathFinding.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//
#include "stdafx.h"
#include "JPS.h"
#include "Heap.h"
#include "PathFinding.h"
#include "my_new.h"
/* ***************************************
매크로
**************************************** */
#define MAP_WIDTH			(200)
#define MAP_HEIGHT			(200)
#define MAP_SCROLL_DISTANCE	(20)

#define TILE_SIZE			(15)
#define DIAGONAL_WEIGHT		(1.5)
#define SRC_NOT_USED		(-1)
#define	DEST_NOT_USED		(-1)

/* ***************************************
구조체
**************************************** */

/* ***************************************
Title map[index_y][index_x]와 같은 방법으로 사용.
여기서 배열 인덱스가 실질적인 (x,y)이고,
Tile의 x,y는 화면에 그려줄 좌표.
**************************************** */
struct Tile
{
	enum eType
	{
		Normal = 0, Blocked, ShallowWater, DeepWater, Visited, Find, Node, Line
	};

	eType		_type;
	int			_x;
	int			_y;
	COLORREF	_RGB;
};

struct Node
{
	float	_G;
	float	_H;
	float	_F;
	int		_x;
	int		_y;
	Node*	_parent;
};

/* ***************************************
함수
**************************************** */
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK MlessDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam);

void Initialize();
void ReInitialize();
void Release();

/* ***************************************
마우스 클릭 좌표를 보고 어떤 타일인지 검색
이후 함수들은, 여기서 구한 x, y를 기준으로 진행
**************************************** */
bool SearchTile(int* const index_x, int* const index_y, int mouse_x, int mouse_y);

void FixSrcTile(int index_x, int index_y);
void FixDestTile(int index_x, int index_y);
void FixDragTile(int index_x, int index_y, bool left_button, bool right_button);
void FixColorAtVisitedTile(int index_x, int index_y);

void Draw(HDC hdc);
void DrawMap(HDC hdc);
void DrawSrcTile(HDC hdc);
void DrawDestTile(HDC hdc);
void DrawDragTile(HDC hdc);
void DrawVisitedTile(HDC hdc);
void DrawSearchingNode(HDC hdc);
void DrawFoundNode(HDC hdc);

void DrawJPSFoundNodeAllPath(HDC hdc);
void DrawJPSFoundNodeNextPath(HDC hdc);
void DrawAStarFoundNodeAllPath(HDC hdc);
void DrawAStarFoundNodeNextPath(HDC hdc);
/* ***************************************
Bresenham 알고리즘을 통한 직선 그리기
**************************************** */
void FixLineByBresenham(int g_src_x, int g_src_y, int g_dest_x, int g_dest_y);
void DrawLineByBresenham(HDC hdc);

/* ***************************************
최소 힙인지, 최대 힙인지 등에 대한
판단 근거는 cmp 함수를 따로 만들어서 제공.
**************************************** */
template <>
bool Heap<Node*>::cmp(Node* node1, Node* node2);

Node* MakeNode(int x, int y, float G, Node* parent);
bool MoveCheck(Node* node);
bool PreCreatedChecker(Node* node);
bool Insert(Node* node);
bool ComparePosWithTile(int x, int y);
bool ComparePosWithDest(int x, int y);

bool SearchRR(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);
bool SearchDD(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);
bool SearchLL(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);
bool SearchUU(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);

void SearchRU(int x, int y, Node* parent);
void SearchRD(int x, int y, Node* parent);
void SearchLU(int x, int y, Node* parent);
void SearchLD(int x, int y, Node* parent);

void JumpRU(int x, int y, Node* parent);
void JumpRD(int x, int y, Node* parent);
void JumpLU(int x, int y, Node* parent);
void JumpLD(int x, int y, Node* parent);

void FindPath();

/* ***************************************
전역 변수
**************************************** */
HINSTANCE	hInst;                                // 현재 인스턴스입니다.
Tile		g_map[MAP_HEIGHT][MAP_WIDTH];
int			g_src_x;
int			g_src_y;
int			g_dest_x;
int			g_dest_y;
int			g_drag_type;

HDC			g_mem_dc;
HBITMAP		g_mem_bitmap;
HBITMAP		g_mem_old_bitmap;
COLORREF	g_visited_color;

RECT		g_rect;
HWND		g_hWnd;
HWND		g_hMDlg;

std::list<Node*> g_close_list;
Heap<Node*>g_open_list(MAP_WIDTH* MAP_HEIGHT);

bool		g_find_path;
Node*		g_find;
float		g_G_weight = 1.0;
float		g_F_weight = 1.0;

bool		g_draw_line = false;
int			g_line_src_x;
int			g_line_src_y;
int			g_line_dest_x;
int			g_line_dest_y;

pathfinding::Pos* g_jps_pos = nullptr;
std::list<pathfinding::Pos*> g_jps_next_test;

pathfinding::Pos* g_astar_pos = nullptr;
std::list<pathfinding::Pos*> g_astar_next_test;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	timeBeginPeriod(1);
	Initialize();
	/* ***************************************
	윈도우 등록
	**************************************** */
	{
		WNDCLASSEXW wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_JPS));
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_JPS);
		wcex.lpszClassName = L"PathFinding";
		wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		RegisterClassExW(&wcex);
	}

	/* ***************************************
	윈도우 띄우기
	**************************************** */
	{
		hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

		HWND hWnd = CreateWindowW(L"PathFinding", L"PathFinding", WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

		if (!hWnd)
		{
			return FALSE;
		}
		g_hWnd = hWnd;

		ShowWindow(hWnd, nCmdShow);
		UpdateWindow(hWnd);
	}

	MSG msg;
	GetClientRect(g_hWnd, &g_rect);

	// 기본 메시지 루프입니다.
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static bool left_button = false;
	static bool right_button = false;
	static bool drag = false;
	static int map_draw_x = TILE_SIZE;
	static int map_draw_y = TILE_SIZE;

	switch (message)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_LEFT:
			map_draw_x -= MAP_SCROLL_DISTANCE;
			map_draw_x = max(0, map_draw_x);
			break;

		case VK_RIGHT:
			if (map_draw_x + MAP_SCROLL_DISTANCE + g_rect.right < MAP_WIDTH * TILE_SIZE)
			{
				map_draw_x += MAP_SCROLL_DISTANCE;
			}
			break;

		case VK_DOWN:
			if (map_draw_y + MAP_SCROLL_DISTANCE + g_rect.bottom < MAP_HEIGHT * TILE_SIZE)
			{
				map_draw_y += MAP_SCROLL_DISTANCE;
			}
			break;

		case VK_UP:
			map_draw_y -= MAP_SCROLL_DISTANCE;
			map_draw_y = max(0, map_draw_y);
			break;

		case 0x51: // q
			g_G_weight += 0.1f;
			break;

		case 0x57: // w
			g_G_weight -= 0.1f;
			break;

		case 0x45: // e
			g_F_weight += 0.1f;
			break;

		case 0x52: // r
			g_F_weight -= 0.1f;
			break;

		default:
			break;
		}

		InvalidateRect(g_hWnd, NULL, false);
		break;

	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 메뉴 선택을 구문 분석합니다.
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;

	case WM_LBUTTONDOWN:
		left_button = true;

		if (g_draw_line)
		{
			SearchTile(&g_line_src_x, &g_line_src_y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y);
		}
		break;

	case WM_RBUTTONDOWN:
		right_button = true;
		break;

	case WM_MOUSEMOVE:
		if (left_button || right_button)
		{
			drag = true;

			int x;
			int y;
			if (SearchTile(&x, &y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y))
			{
				FixDragTile(x, y, left_button, right_button);
				InvalidateRect(hWnd, NULL, false);
			}
		}
		break;

	case WM_LBUTTONUP:
	{
		ReInitialize();
		left_button = false;

		if (g_draw_line)
		{
			if (SearchTile(&g_line_dest_x, &g_line_dest_y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y))
			{
				FixLineByBresenham(g_line_src_x, g_line_src_y, g_line_dest_x, g_line_dest_y);
			}
		}

		if (drag)
		{
			drag = false;
			InvalidateRect(hWnd, NULL, false);

			break;
		}

		int x;
		int y;
		if (SearchTile(&x, &y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y))
		{
			FixSrcTile(x, y);
			InvalidateRect(hWnd, NULL, false);
		}
	}
	break;

	case WM_RBUTTONUP:
	{
		ReInitialize();
		right_button = false;

		if (drag)
		{
			drag = false;
			InvalidateRect(hWnd, NULL, false);

			break;
		}

		int x;
		int y;
		if (SearchTile(&x, &y, LOWORD(lParam) + map_draw_x, HIWORD(lParam) + map_draw_y))
		{
			FixDestTile(x, y);
			InvalidateRect(hWnd, NULL, false);
		}
	}
	break;
	
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		if (g_find_path)
		{
			FindPath();

			InvalidateRect(hWnd, NULL, false);
		}

		Draw(g_mem_dc);

		BitBlt(hdc, 0, 0, g_rect.right, g_rect.bottom, g_mem_dc, map_draw_x, map_draw_y, SRCCOPY);

		EndPaint(hWnd, &ps);
	}
	break;

	case WM_SIZE:
		GetClientRect(g_hWnd, &g_rect);
		break;

	case WM_DESTROY:
		SelectObject(g_mem_dc, g_mem_old_bitmap);
		DeleteObject(g_mem_bitmap);
		DeleteDC(g_mem_dc);
		timeEndPeriod(1);
		PostQuitMessage(0);
		break;

	case WM_CREATE:
	{
		HDC hdc = GetDC(hWnd);
		g_mem_dc = CreateCompatibleDC(hdc);
		g_mem_bitmap = CreateCompatibleBitmap(hdc, MAP_WIDTH * TILE_SIZE, MAP_HEIGHT * TILE_SIZE);
		g_mem_old_bitmap = (HBITMAP)SelectObject(g_mem_dc, g_mem_bitmap);
		DeleteDC(hdc);

		if (!IsWindow(g_hMDlg))
		{
			g_hMDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, MlessDlgProc);
			ShowWindow(g_hMDlg, SW_SHOW);
		}
	}
	break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

INT_PTR CALLBACK MlessDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDC_BUTTON1: // 화면 지우기
			Release();
			InvalidateRect(g_hWnd, NULL, false);
			break;

		case IDC_BUTTON2: // 길 찾기 시작
		{
			ReInitialize();
			g_find_path = true;

			Node* start = MakeNode(g_src_x, g_src_y, 0, nullptr);

			g_open_list.push(start);

			InvalidateRect(g_hWnd, NULL, false);
		}
		break;

		case IDC_BUTTON3: // 일시정지
			if (g_find)
			{
				break;
			}

			if (g_find_path)
			{
				SetDlgItemText(hDlg, IDC_BUTTON3, L"재개");
			}
			else
			{
				SetDlgItemText(hDlg, IDC_BUTTON3, L"일시 정지");
			}
			g_find_path = !g_find_path;
			InvalidateRect(g_hWnd, NULL, false);

			break;

		case IDC_BUTTON4:
			ReInitialize();

			for (int row = 0; row < MAP_HEIGHT; row++)
			{
				for (int col = 0; col < MAP_WIDTH; col++)
				{
					if (g_map[row][col]._type == Tile::eType::Visited)
					{
						g_map[row][col]._type = Tile::eType::Normal;
					}
				}
			}

			InvalidateRect(g_hWnd, NULL, false);

			break;

		case IDC_BUTTON5:		
			if (g_draw_line)
			{
				SetDlgItemText(hDlg, IDC_BUTTON5, L"직선 OFF");
			}
			else
			{
				SetDlgItemText(hDlg, IDC_BUTTON5, L"직선 ON");
			}

			g_draw_line = !g_draw_line;

			break;

		case IDC_BUTTON6:
		{
			ReInitialize();

			pathfinding::JPS jps(1.0, 1.0, 100, ComparePosWithTile);
			jps.GetAllPath(&g_jps_pos, g_src_x, g_src_y, g_dest_x, g_dest_y);
			InvalidateRect(g_hWnd, NULL, false);
		}
		break;

		case IDC_BUTTON7:
		{
			ReInitialize();

			pathfinding::JPS jps(1.0, 1.0, 100, ComparePosWithTile);

			int next_x;
			int next_y;
			int start_x = g_src_x;
			int start_y = g_src_y;
			while (1)
			{
				if (jps.GetNextPath(&next_x, &next_y, start_x, start_y, g_dest_x, g_dest_y))
				{
					pathfinding::Pos* pos = new pathfinding::Pos;
					pos->_x = next_x;
					pos->_y = next_y;
					g_jps_next_test.push_back(pos);
					start_x = next_x;
					start_y = next_y;

					if (next_x == g_dest_x && next_y == g_dest_y)
					{
						break;
					}
				}
				else
				{
					break;
				}

			}
			InvalidateRect(g_hWnd, NULL, false);
		}
		break;

		case IDC_BUTTON8:
		{
			ReInitialize();

			pathfinding::AStar astar(1.0, 1.0, 500, ComparePosWithTile);
			astar.GetAllPath(&g_astar_pos, g_src_x, g_src_y, g_dest_x, g_dest_y);
			InvalidateRect(g_hWnd, NULL, false);
		}
		break;

		case IDC_BUTTON9:
		{
			ReInitialize();

			pathfinding::AStar astar(1.0, 1.0, 500, ComparePosWithTile);

			int next_x;
			int next_y;
			int start_x = g_src_x;
			int start_y = g_src_y;
			while (1)
			{
				if (astar.GetNextPath(&next_x, &next_y, start_x, start_y, g_dest_x, g_dest_y))
				{
					pathfinding::Pos* pos = new pathfinding::Pos;
					pos->_x = next_x;
					pos->_y = next_y;
					g_astar_next_test.push_back(pos);
					start_x = next_x;
					start_y = next_y;

					if (next_x == g_dest_x && next_y == g_dest_y)
					{
						break;
					}
				}
				else
				{
					break;
				}

			}
			InvalidateRect(g_hWnd, NULL, false);
		}
		break;
		default:

			break;
		}
		default:
			break;
		}

	}

	return false;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void Initialize()
{
	g_src_x = SRC_NOT_USED;
	g_src_y = SRC_NOT_USED;
	g_dest_x = DEST_NOT_USED;
	g_dest_y = DEST_NOT_USED;

	g_mem_dc = nullptr;
	g_mem_bitmap = nullptr;
	g_mem_old_bitmap = nullptr;

	g_find_path = false;
	g_drag_type = Tile::eType::Blocked;
	g_find = nullptr;

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			g_map[y][x]._type = Tile::eType::Normal;
			g_map[y][x]._x = x * TILE_SIZE;
			g_map[y][x]._y = y * TILE_SIZE;
		}
	}
}

void ReInitialize()
{
	g_find_path = false;

	size_t size = g_open_list.size();
	for (size_t index = 0; index < size; index++)
	{
		delete g_open_list[index];
	}

	for (auto e : g_close_list)
	{
		delete e;
	}

	g_open_list.clear();
	g_close_list.clear();

	g_find = nullptr;

	if (g_jps_pos)
	{
		delete[] g_jps_pos;
		g_jps_pos = nullptr;
	}
	g_jps_next_test.clear();

	if (g_astar_pos)
	{
		delete[] g_astar_pos;
		g_astar_pos = nullptr;
	}

	g_astar_next_test.clear();
}

void Release()
{
	ReInitialize();
	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			g_map[y][x]._type = Tile::eType::Normal;
			g_map[y][x]._x = x * TILE_SIZE;
			g_map[y][x]._y = y * TILE_SIZE;
		}
	}
}

bool SearchTile(int* const index_x, int* const index_y, int mouse_x, int mouse_y)
{
	int x = mouse_x / TILE_SIZE;
	int y = mouse_y / TILE_SIZE;

	if (x >= 0 && x < MAP_WIDTH && y >= 0 && y < MAP_HEIGHT)
	{
		*index_x = mouse_x / TILE_SIZE;
		*index_y = mouse_y / TILE_SIZE;

		return true;
	}

	return false;
}

void FixSrcTile(int index_x, int index_y)
{
	if (g_src_x == index_x && g_src_y == index_y)
	{
		g_src_x = SRC_NOT_USED;
		g_src_y = SRC_NOT_USED;

		return;
	}

	g_src_x = index_x;
	g_src_y = index_y;
}

void FixDestTile(int index_x, int index_y)
{
	if (g_dest_x == index_x && g_dest_y == index_y)
	{
		g_dest_x = DEST_NOT_USED;
		g_dest_y = DEST_NOT_USED;

		return;
	}

	g_dest_x = index_x;
	g_dest_y = index_y;
}

void FixDragTile(int index_x, int index_y, bool left_button, bool right_button)
{
	if ((index_x == g_dest_x && index_y == g_dest_y) || (index_x == g_src_x && index_y == g_src_y))
	{
		return;
	}

	if (right_button)
	{
		g_map[index_y][index_x]._type = Tile::eType::Normal;
	}
	else if (left_button)
	{
		switch (g_drag_type)
		{
		case Tile::eType::Blocked:
			g_map[index_y][index_x]._type = Tile::eType::Blocked;
			break;

		case Tile::eType::ShallowWater:
			g_map[index_y][index_x]._type = Tile::eType::ShallowWater;
			break;

		case Tile::eType::DeepWater:
			g_map[index_y][index_x]._type = Tile::eType::DeepWater;
			break;

		default:
			assert(0);
			break;
		}
	}
	else
	{

	}
}

void FixColorAtVisitedTile(int index_x, int index_y)
{
	g_map[index_y][index_x]._type = Tile::eType::Visited;
	g_map[index_y][index_x]._RGB = g_visited_color;
}

void Draw(HDC hdc)
{
	/* ***************************************
	TODO  :Map 출력과 DragTile출력하는 부분은
	하나로 묶을 수도 있을듯.
	**************************************** */
	DrawMap(hdc);
	DrawDragTile(hdc);
	DrawVisitedTile(hdc);
	DrawSearchingNode(hdc);
	DrawSrcTile(hdc);
	DrawDestTile(hdc);
	DrawFoundNode(hdc);
	
	DrawLineByBresenham(hdc);
	DrawJPSFoundNodeAllPath(hdc);
	DrawJPSFoundNodeNextPath(hdc);
	DrawAStarFoundNodeAllPath(hdc);
	DrawAStarFoundNodeNextPath(hdc);
}

void DrawMap(HDC hdc)
{
	HBRUSH background = CreateSolidBrush(RGB(215, 215, 215));
	HPEN lattice_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));

	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, background);
	HPEN old_pen = (HPEN)SelectObject(hdc, lattice_line);

	Rectangle(hdc, 0, 0, MAP_WIDTH * TILE_SIZE, MAP_HEIGHT*TILE_SIZE);

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		MoveToEx(hdc, g_map[y][0]._x, g_map[y][0]._y, NULL);
		LineTo(hdc, MAP_WIDTH* TILE_SIZE, g_map[y][0]._y);
	}

	for (int x = 0; x < MAP_WIDTH; x++)
	{
		MoveToEx(hdc, g_map[0][x]._x, g_map[0][x]._y, NULL);
		LineTo(hdc, g_map[0][x]._x, MAP_HEIGHT*TILE_SIZE);
	}

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);

	DeleteObject(background);
	DeleteObject(lattice_line);
}

void DrawSrcTile(HDC hdc)
{
	if (g_src_x == SRC_NOT_USED)
	{
		return;
	}

	HBRUSH slot_background = CreateSolidBrush(RGB(34, 177, 76));
	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, slot_background);
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));
	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);

	int src_x = g_map[g_src_y][g_src_x]._x;
	int src_y = g_map[g_src_y][g_src_x]._y;

	Rectangle(hdc, src_x, src_y, src_x + TILE_SIZE, src_y + TILE_SIZE);

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);
	DeleteObject(out_line);
	DeleteObject(slot_background);
}

void DrawDestTile(HDC hdc)
{
	if (g_dest_x == DEST_NOT_USED)
	{
		return;
	}

	HBRUSH slot_background = CreateSolidBrush(RGB(255, 0, 0));
	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, slot_background);
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));
	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);
	int dest_x = g_map[g_dest_y][g_dest_x]._x;
	int dest_y = g_map[g_dest_y][g_dest_x]._y;

	Rectangle(hdc, dest_x, dest_y, dest_x + TILE_SIZE, dest_y + TILE_SIZE);

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);
	DeleteObject(out_line);
	DeleteObject(slot_background);
}

void DrawDragTile(HDC hdc)
{
	HBRUSH blocked = CreateSolidBrush(RGB(100, 100, 100));
	HBRUSH shallow_water = CreateSolidBrush(RGB(153, 217, 234));
	HBRUSH deep_water = CreateSolidBrush(RGB(0, 162, 232));
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));

	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);
	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, blocked);

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			switch (g_map[y][x]._type)
			{
			case Tile::eType::Blocked:
			{
				SelectObject(hdc, blocked);
				int blocked_x = g_map[y][x]._x;
				int blocked_y = g_map[y][x]._y;

				Rectangle(hdc, blocked_x, blocked_y, blocked_x + TILE_SIZE, blocked_y + TILE_SIZE);
			}
			break;

			case Tile::eType::ShallowWater:
			{
				SelectObject(hdc, shallow_water);
				int blocked_x = g_map[y][x]._x;
				int blocked_y = g_map[y][x]._y;

				Rectangle(hdc, blocked_x, blocked_y, blocked_x + TILE_SIZE, blocked_y + TILE_SIZE);
			}
			break;

			case Tile::eType::DeepWater:
			{
				SelectObject(hdc, deep_water);
				int blocked_x = g_map[y][x]._x;
				int blocked_y = g_map[y][x]._y;

				Rectangle(hdc, blocked_x, blocked_y, blocked_x + TILE_SIZE, blocked_y + TILE_SIZE);
			}
			break;

			default:

				break;
			}
		}
	}

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);
	DeleteObject(out_line);
	DeleteObject(blocked);
	DeleteObject(deep_water);
	DeleteObject(shallow_water);
}

void DrawVisitedTile(HDC hdc)
{
	int r = rand() % 255;
	int g = rand() % 255;
	int b = rand() % 255;

	HBRUSH visited = CreateSolidBrush(RGB(r, g, b));
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));

	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);
	HBRUSH old_brush = nullptr;

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			switch (g_map[y][x]._type)
			{
			case Tile::eType::Visited:
			{
				int visited_x = g_map[y][x]._x;
				int visited_y = g_map[y][x]._y;

				COLORREF color = g_map[y][x]._RGB;
				visited = CreateSolidBrush(color);
				old_brush = (HBRUSH)SelectObject(hdc, visited);

				Rectangle(hdc, visited_x, visited_y, visited_x + TILE_SIZE, visited_y + TILE_SIZE);

				SelectObject(hdc, old_brush);
				DeleteObject(visited);
			}
			break;

			default:
				break;
			}
		}
	}

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);
	DeleteObject(out_line);
	DeleteObject(visited);
}

void DrawFoundNode(HDC hdc)
{
	if (!g_find)
	{
		return;
	}

	// 직선용 코드
	Node* find = g_find;
	Node* parent = find->_parent;

	while (find && parent)
	{
		int parent_x = parent->_x;
		int parent_y = parent->_y;
		int x = find->_x;
		int y = find->_y;

		float width = (float)(x - parent_x);
		float height = (float)(y - parent_y);
		int step = 2 * (int)max(abs(width), abs(height));

		width /= step;
		height /= step;

		bool check = true;

		for (int count = 1; count <= step; count++)
		{
			if (g_map[(int)round(parent_y + height*count)][(int)round(parent_x + width*count)]._type == Tile::eType::Blocked)
			{
				check = false;

				break;
			}
		}

		if (check)
		{
			find->_parent = parent;
			parent = parent->_parent;
		}
		else
		{
			find = parent;
			parent = parent->_parent;
		}
	}

	HPEN find_color = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	HPEN old_pen = (HPEN)SelectObject(hdc, find_color);

	find = g_find;
	MoveToEx(hdc, g_map[g_dest_y][g_dest_x]._x + TILE_SIZE / 2, g_map[g_dest_y][g_dest_y]._x + TILE_SIZE / 2, NULL);

	while (g_find)
	{
		int x = g_map[g_find->_y][g_find->_x]._x;
		int y = g_map[g_find->_y][g_find->_x]._y;

		LineTo(hdc, x + TILE_SIZE / 2, y + TILE_SIZE / 2);
		MoveToEx(hdc, x + TILE_SIZE / 2, y + TILE_SIZE / 2, NULL);
		g_find = g_find->_parent;
	}

	g_find = find;

	SelectObject(hdc, old_pen);
	DeleteObject(find_color);
}

void DrawJPSFoundNodeAllPath(HDC hdc)
{
	if (!g_jps_pos)
	{
		return;
	}

	HPEN find_color = CreatePen(PS_SOLID, 3, RGB(0, 0, 255));
	HPEN old_pen = (HPEN)SelectObject(hdc, find_color);

	MoveToEx(hdc, g_map[g_src_y][g_src_x]._x + TILE_SIZE / 2, g_map[g_src_y][g_src_x]._y + TILE_SIZE / 2, NULL);
	
	int index = 0;
	while (1)
	{
		int x = g_jps_pos[index]._x;
		int y = g_jps_pos[index]._y;
		
		int draw_x = g_map[y][x]._x;
		int draw_y = g_map[y][x]._y;

		LineTo(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2);
		MoveToEx(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2, NULL);

		index++;

		if (ComparePosWithDest(x, y))
		{
			break;
		}
	}

	SelectObject(hdc, old_pen);
	DeleteObject(find_color);

}

void DrawJPSFoundNodeNextPath(HDC hdc)
{
	if (g_jps_next_test.empty())
	{
		return;
	}

	HPEN find_color = CreatePen(PS_SOLID, 3, RGB(0, 255, 0));
	HPEN old_pen = (HPEN)SelectObject(hdc, find_color);

	MoveToEx(hdc, g_map[g_src_y][g_src_x]._x + TILE_SIZE / 2, g_map[g_src_y][g_src_x]._y + TILE_SIZE / 2, NULL);

	int index = 0;
	while (!g_jps_next_test.empty())
	{
		int x = g_jps_next_test.front()->_x;
		int y = g_jps_next_test.front()->_y;
		g_jps_next_test.pop_front();

		int draw_x = g_map[y][x]._x;
		int draw_y = g_map[y][x]._y;

		LineTo(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2);
		MoveToEx(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2, NULL);
		if (ComparePosWithDest(x, y))
		{
			break;
		}
	}

	SelectObject(hdc, old_pen);
	DeleteObject(find_color);
}

void DrawAStarFoundNodeAllPath(HDC hdc)
{
	if (!g_astar_pos)
	{
		return;
	}

	HPEN find_color = CreatePen(PS_SOLID, 3, RGB(0, 0, 255));
	HPEN old_pen = (HPEN)SelectObject(hdc, find_color);

	MoveToEx(hdc, g_map[g_src_y][g_src_x]._x + TILE_SIZE / 2, g_map[g_src_y][g_src_x]._y + TILE_SIZE / 2, NULL);

	int index = 0;
	while (1)
	{
		int x = g_astar_pos[index]._x;
		int y = g_astar_pos[index]._y;

		int draw_x = g_map[y][x]._x;
		int draw_y = g_map[y][x]._y;

		LineTo(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2);
		MoveToEx(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2, NULL);

		index++;

		if (ComparePosWithDest(x, y))
		{
			break;
		}
	}

	SelectObject(hdc, old_pen);
	DeleteObject(find_color);
}

void DrawAStarFoundNodeNextPath(HDC hdc)
{
	if (g_astar_next_test.empty())
	{
		return;
	}

	HPEN find_color = CreatePen(PS_SOLID, 3, RGB(0, 255, 0));
	HPEN old_pen = (HPEN)SelectObject(hdc, find_color);

	MoveToEx(hdc, g_map[g_src_y][g_src_x]._x + TILE_SIZE / 2, g_map[g_src_y][g_src_x]._y + TILE_SIZE / 2, NULL);

	int index = 0;
	while (!g_astar_next_test.empty())
	{
		int x = g_astar_next_test.front()->_x;
		int y = g_astar_next_test.front()->_y;
		g_astar_next_test.pop_front();

		int draw_x = g_map[y][x]._x;
		int draw_y = g_map[y][x]._y;

		LineTo(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2);
		MoveToEx(hdc, draw_x + TILE_SIZE / 2, draw_y + TILE_SIZE / 2, NULL);
		if (ComparePosWithDest(x, y))
		{
			break;
		}
	}

	SelectObject(hdc, old_pen);
	DeleteObject(find_color);
}

/* ***************************************
	가로로 아주 긴 선의 경우 원하던 모습이 안나온다.
	ex)
														ㅁ
	ㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁ

	이 부분을 보정하기 위해서 최초의 error 값을 0으로 시작하지 않고
	긴 축의 절반만큼을 더해준다.
**************************************** */
void FixLineByBresenham(int src_x, int src_y, int dest_x, int dest_y)
{
	int dif_x = dest_x - src_x;
	int dif_y = dest_y - src_y;
	int abs_dif_x = abs(dif_x);
	int abs_dif_y = abs(dif_y);
	int direct_x = 0;
	int direct_y = 0;
	
	if (abs_dif_x != 0)
	{
		direct_x = dif_x / abs_dif_x;
	}

	if (abs_dif_y != 0)
	{
		direct_y = dif_y / abs_dif_y;
	}
	
	if (abs_dif_x > abs_dif_y)
	{
		int error = abs_dif_x / 2;
		
		g_map[src_y][src_x]._type = Tile::eType::Line;

		for (int index = 0; index < abs_dif_x; index++)
		{
			error += abs_dif_y;

			if (error >= abs_dif_x)
			{
				src_y += direct_y;
				error -= abs_dif_x;
			}

			src_x += direct_x;
			g_map[src_y][src_x]._type = Tile::eType::Line;
		}
	}
	else
	{
		int error = abs_dif_y / 2;

		g_map[src_y][src_x]._type = Tile::eType::Line;
		for (int index = 0; index < abs_dif_y; index++)
		{
			error += abs_dif_x;

			if (error >= abs_dif_y)
			{
				src_x += direct_x;
				error -= abs_dif_y;
			}

			src_y += direct_y;
			g_map[src_y][src_x]._type = Tile::eType::Line;
		}
	}
}

void DrawLineByBresenham(HDC hdc)
{
	HBRUSH visited = CreateSolidBrush(RGB(255, 0, 0));
	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(230, 230, 230));
	HPEN red_line = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));

	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);
	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, visited);

	for (int y = 0; y < MAP_HEIGHT; y++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			switch (g_map[y][x]._type)
			{
			case Tile::eType::Line:
			{
				Rectangle(hdc, g_map[y][x]._x, g_map[y][x]._y, g_map[y][x]._x + TILE_SIZE, g_map[y][x]._y + TILE_SIZE);
			}
			break;

			default:
				break;
			}
		}
	}

	MoveToEx(hdc, g_map[g_line_src_y][g_line_src_x]._x + TILE_SIZE / 2, g_map[g_line_src_y][g_line_src_x]._y + TILE_SIZE / 2, NULL);
	LineTo(hdc, g_map[g_line_dest_y][g_line_dest_x]._x + TILE_SIZE / 2 , g_map[g_line_dest_y][g_line_dest_x]._y + TILE_SIZE / 2);

	SelectObject(hdc, old_pen);
	SelectObject(hdc, old_brush);

	DeleteObject(red_line);
	DeleteObject(out_line);
	DeleteObject(visited);
}

void DrawSearchingNode(HDC hdc)
{
	HBRUSH close_color = CreateSolidBrush(RGB(255, 201, 14));
	HBRUSH open_color = CreateSolidBrush(RGB(13, 19, 107));

	HPEN out_line = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));

	HBRUSH old_brush = (HBRUSH)SelectObject(hdc, close_color);
	HPEN old_pen = (HPEN)SelectObject(hdc, out_line);

	for (Node* node : g_close_list)
	{
		int x = g_map[node->_y][node->_x]._x;
		int y = g_map[node->_y][node->_x]._y;

		Rectangle(hdc, x, y, x + TILE_SIZE, y + TILE_SIZE);
	}

	SelectObject(hdc, open_color);

	size_t size = g_open_list.size();
	for (size_t index = 0; index < size; index++)
	{
		Node* node = g_open_list[index];

		int x = g_map[node->_y][node->_x]._x;
		int y = g_map[node->_y][node->_x]._y;

		Rectangle(hdc, x, y, x + TILE_SIZE, y + TILE_SIZE);
	}

	SelectObject(hdc, old_brush);
	SelectObject(hdc, old_pen);

	DeleteObject(out_line);
	DeleteObject(open_color);
	DeleteObject(close_color);
}

/* ***************************************
최소 힙인지, 최대 힙인지 등에 대한
판단 근거는 cmp 함수를 따로 만들어서 제공.
**************************************** */
template <>
bool Heap<Node*>::cmp(Node* node1, Node* node2)
{
	return node1->_F < node2->_F;
}

Node* MakeNode(int x, int y, float G, Node* parent)
{
	Node* node = new Node;

	float tile_weight = 1.0f;

	switch (g_map[y][x]._type)
	{
	case Tile::eType::DeepWater:
		tile_weight = 1.7f;
		break;
	case Tile::eType::ShallowWater:
		tile_weight = 1.2f;
		break;

	default:

		break;
	}

	node->_x = x;
	node->_y = y;
	node->_G = G * g_G_weight;
	node->_H = (float)((float)abs(node->_x - g_dest_x) + (float)abs(node->_y - g_dest_y));
	node->_F = (float)((node->_G + node->_H) * g_F_weight * tile_weight);
	node->_parent = parent;

	return node;
}

bool MoveCheck(Node* node)
{
	int x = node->_x;
	int y = node->_y;

	if (x >= 0 && x < MAP_WIDTH && y >= 0 && y < MAP_HEIGHT)
	{
		if (g_map[y][x]._type != Tile::eType::Blocked)
		{
			return true;
		}
	}

	return false;
}

bool PreCreatedChecker(Node* node)
{
	int size = (int)g_open_list.size();

	for (int index = 0; index < size; index++)
	{
		if (node->_x == g_open_list[index]->_x && node->_y == g_open_list[index]->_y)
		{
			return false;
		}
	}

	for (Node* n : g_close_list)
	{
		if (n->_x == node->_x && n->_y == node->_y)
		{
			return false;
		}
	}

	return true;
}

bool Insert(Node* node)
{
	if (MoveCheck(node) && PreCreatedChecker(node))
	{
		g_open_list.push(node);

		return true;
	}

	delete node;

	return false;
}

bool ComparePosWithTile(int x, int y)
{
	if (g_map[y][x]._type == Tile::eType::Blocked || x <= 0 || x >= MAP_WIDTH || y <= 0 || y >= MAP_HEIGHT)
	{
		return false;
	}

	return true;
}

bool ComparePosWithDest(int x, int y)
{
	if (x == g_dest_x && y == g_dest_y)
	{
		return true;
	}

	return false;
}

bool SearchRR(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!ComparePosWithTile(x, y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			if (Insert(node))
			{
				g_find = node;
				g_find_path = false;
			}
		}

		return true;
	}

	FixColorAtVisitedTile(x, y);

	bool b_right_up = (g_map[y - 1][x]._type == Tile::eType::Blocked && (g_map[y - 1][x + 1]._type != Tile::eType::Blocked));
	bool b_right_down = (g_map[y + 1][x]._type == Tile::eType::Blocked && (g_map[y + 1][x + 1]._type != Tile::eType::Blocked));

	if(b_right_up || b_right_down)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_right_up)
				{
					JumpRU(x + 1, y - 1, node_parent);
				}

				if (b_right_down)
				{
					JumpRD(x + 1, y + 1, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchRR(x + 1, y, parent, b_only_horizontal_vertical, false);
}

bool SearchDD(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!ComparePosWithTile(x, y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			if (Insert(node))
			{
				g_find = node;
				g_find_path = false;
			}
		}

		return true;
	}

	FixColorAtVisitedTile(x, y);

	bool b_left_down = (g_map[y][x - 1]._type == Tile::eType::Blocked && (g_map[y + 1][x - 1]._type != Tile::eType::Blocked));
	bool b_right__down = (g_map[y][x + 1]._type == Tile::eType::Blocked && (g_map[y + 1][x + 1]._type != Tile::eType::Blocked));

	if(b_left_down || b_right__down)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_left_down)
				{
					JumpLD(x - 1, y + 1, node_parent);
				}

				if (b_right__down)
				{
					JumpRD(x + 1, y + 1, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchDD(x, y + 1, parent, b_only_horizontal_vertical, false);
}

bool SearchLL(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!ComparePosWithTile(x, y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			if (Insert(node))
			{
				g_find = node;
				g_find_path = false;
			}
		}

		return true;
	}

	FixColorAtVisitedTile(x, y);

	bool b_left_up = (g_map[y - 1][x]._type == Tile::eType::Blocked && (g_map[y - 1][x - 1]._type != Tile::eType::Blocked));
	bool b_left_down = (g_map[y + 1][x]._type == Tile::eType::Blocked && (g_map[y + 1][x - 1]._type != Tile::eType::Blocked));

	if(b_left_down || b_left_up)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_left_up)
				{
					JumpLU(x - 1, y - 1, node_parent);
				}

				if (b_left_down)
				{
					JumpLD(x - 1, y + 1, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchLL(x - 1, y, parent, b_only_horizontal_vertical, false);
}

bool SearchUU(int x, int y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours)
{
	if (!ComparePosWithTile(x, y))
	{
		return false;
	}

	if (ComparePosWithDest(x, y))
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			if (Insert(node))
			{
				g_find = node;
				g_find_path = false;
			}
		}

		return true;
	}

	FixColorAtVisitedTile(x, y);

	bool b_left_up = (g_map[y][x - 1]._type == Tile::eType::Blocked && (g_map[y - 1][x - 1]._type != Tile::eType::Blocked));
	bool b_right_up = (g_map[y][x + 1]._type == Tile::eType::Blocked && (g_map[y - 1][x + 1]._type != Tile::eType::Blocked));

	if(b_left_up || b_right_up)
	{
		if (b_only_horizontal_vertical)
		{
			Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
			Node* node_parent = nullptr;

			if (Insert(node))
			{
				node_parent = node;
			}
			else
			{
				node_parent = parent;
			}

			if (b_create_forced_neighbours)
			{
				if (b_left_up)
				{
					JumpLU(x - 1, y - 1, node_parent);
				}

				if (b_right_up)
				{
					JumpRU(x + 1, y - 1, node_parent);
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}

	return SearchUU(x, y - 1, parent, b_only_horizontal_vertical, false);
}

void SearchRU(int x, int y, Node* parent)
{
	SearchRR(x, y, parent, true, true);
	SearchUU(x, y, parent, true, true);

	JumpRU(x + 1, y - 1, parent);
}

void SearchRD(int x, int y, Node* parent)
{
	SearchRR(x, y, parent, true, true);
	SearchDD(x, y, parent, true, true);

	JumpRD(x + 1, y + 1, parent);
}

void SearchLU(int x, int y, Node* parent)
{
	SearchLL(x, y, parent, true, true);
	SearchUU(x, y, parent, true, true);

	JumpLU(x - 1, y - 1, parent);
}

void SearchLD(int x, int y, Node* parent)
{
	SearchLL(x, y, parent, true, true);
	SearchDD(x, y, parent, true, true);

	JumpLD(x - 1, y + 1, parent);
}

void JumpRU(int x, int y, Node* parent)
{
	if (!ComparePosWithTile(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		if (Insert(node))
		{
			g_find = node;
			g_find_path = false;
		}

		return;
	}

	FixColorAtVisitedTile(x, y);

	if ((g_map[y][x - 1]._type == Tile::eType::Blocked && (g_map[y - 1][x - 1]._type != Tile::eType::Blocked))
		|| (g_map[y + 1][x]._type == Tile::eType::Blocked && (g_map[y + 1][x + 1]._type != Tile::eType::Blocked)))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchRR(x + 1, y, parent, false, false) || SearchUU(x, y - 1, parent, false, false))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	JumpRU(x + 1, y - 1, parent);
}

void JumpRD(int x, int y, Node* parent)
{
	if (!ComparePosWithTile(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		if (Insert(node))
		{
			g_find = node;
			g_find_path = false;
		}

		return;
	}

	FixColorAtVisitedTile(x, y);

	if ( (g_map[y - 1][x]._type == Tile::eType::Blocked && (g_map[y - 1][x + 1]._type != Tile::eType::Blocked))
		|| (g_map[y][x - 1]._type == Tile::eType::Blocked && (g_map[y + 1][x - 1]._type != Tile::eType::Blocked)))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchRR(x + 1, y, parent, false, false) || SearchDD(x, y + 1, parent, false, false))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	JumpRD(x + 1, y + 1, parent);
}

void JumpLU(int x, int y, Node* parent)
{
	if (!ComparePosWithTile(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		if (Insert(node))
		{
			g_find = node;
			g_find_path = false;
		}

		return;
	}

	FixColorAtVisitedTile(x, y);

	if ((g_map[y][x + 1]._type == Tile::eType::Blocked && (g_map[y - 1][x + 1]._type != Tile::eType::Blocked))
		|| (g_map[y + 1][x]._type == Tile::eType::Blocked && (g_map[y + 1][x - 1]._type != Tile::eType::Blocked)))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchLL(x - 1, y, parent, false, false) || SearchUU(x, y - 1, parent, false, false))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	JumpLU(x - 1, y - 1, parent);
}

void JumpLD(int x, int y, Node* parent)
{
	if (!ComparePosWithTile(x, y))
	{
		return;
	}

	if (ComparePosWithDest(x, y))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		if (Insert(node))
		{
			g_find = node;
			g_find_path = false;
		}

		return;
	}

	FixColorAtVisitedTile(x, y);

	if ((g_map[y - 1][x]._type == Tile::eType::Blocked && (g_map[y - 1][x - 1]._type != Tile::eType::Blocked))
		|| (g_map[y][x + 1]._type == Tile::eType::Blocked && (g_map[y + 1][x + 1]._type != Tile::eType::Blocked)))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	if (SearchLL(x - 1, y, parent, false, false) || SearchDD(x, y + 1, parent, false, false))
	{
		Node* node = MakeNode(x, y, (float)abs(parent->_x - (x)) + (float)abs(parent->_y - (y)), parent);
		Insert(node);

		return;
	}

	JumpLD(x - 1, y + 1, parent);
}

void FindPath()
{
	if (g_open_list.empty() || g_find)
	{
		g_find_path = false;

		return;
	}

	g_visited_color = RGB(rand() % 255, rand() % 255, rand() % 255);

	Node* node = g_open_list.peek();
	g_open_list.pop();
	g_close_list.push_back(node);

	if (node->_parent)
	{
		int x = node->_x;
		int y = node->_y;
		int parent_x = node->_parent->_x;
		int parent_y = node->_parent->_y;

		int left_or_right = x - parent_x;
		int up_or_down = y - parent_y;

		if (left_or_right > 0)
		{
			if (up_or_down < 0)
			{
				SearchRU(x, y, node);
			}
			else if (up_or_down == 0)
			{
				SearchRR(x, y, node, true, true);
			}
			else
			{
				SearchRD(x, y, node);
			}
		}
		else if (left_or_right == 0)
		{
			if (up_or_down < 0)
			{
				SearchUU(x, y, node, true, true);
			}
			else
			{
				SearchDD(x, y, node, true, true);
			}
		}
		else
		{
			if (up_or_down < 0)
			{
				SearchLU(x, y, node);
			}
			else if (up_or_down == 0)
			{
				SearchLL(x, y, node, true, true);
			}
			else
			{
				SearchLD(x, y, node);
			}
		}
	}
	else
	{
		SearchUU(node->_x, node->_y, node, true, true);
		SearchRR(node->_x, node->_y, node, true, true);
		SearchDD(node->_x, node->_y, node, true, true);
		SearchLL(node->_x, node->_y, node, true, true);
		SearchLU(node->_x, node->_y, node);
		SearchRU(node->_x, node->_y, node);
		SearchRD(node->_x, node->_y, node);
		SearchLD(node->_x, node->_y, node);
	}
}