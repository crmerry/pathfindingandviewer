#pragma once

/* ***************************************
	길찾기 라이브러리
	1. JPS
	2. A*

	[공통 사항]
	 - pathfinding::JPS jps(g가중치, f가중치, 오픈리스트 크기, 타일체크함수);
	 와 같이 초기화 하여 사용한다.
	 - 오픈리스트 크기가 클 수록 더 많은 범위를 찾는 시도를 함.
	 - 타일체크함수는 bool 함수(int pos_x, int pos_y); 와 같은 형식이어야함.

	[사용 방법]
	1. GetNextPath(....)
	jps.GetNextPath(&next_x, &next_y, start_x, start_y, dest_x, dest_y)와 같이 호출하여
	성공하면,
	(start_x, start_y)로 부터 (dest_x, dest_y)까지 갈 수 있는 path의 첫 번째 path가 (next_x, next_y)로 저장됨.
	계속해서 길을 찾고 싶다면, next_x와 next_y를 start_x, start_y에 대입하여 반복적으로 호출.
	next_x와 next_y가 dest_x dest_y와 같은 시점에서 반복을 중지하면됨.
	실패하면,
	출발지에서 목적지까지 갈 수 있는 길이 없거나 오픈 리스트의 크기가 너무 적다(즉, 너무 멀고 장애물이 많은..)
	이 경우, 오픈 리스트의 크기를 확인하거나, 목적지가 막힌 길임을 확인.

	2. GetAllPath(....)
	pathfinding::Pos* pos = nulptr;
	jpg.GetAllPath(&pos,....)와 같이 호출하며
	성공하면,
	Pos 배열에 dest 까지의 경로가 저장됨.
	Pos 자료구조는 int x, int y를 지닌 간단한 구조.

	NOTE: 2018/01/19
	최초에는 Heap을 만들어서 사용 했으나,
	이후 stl의 vector로 바꾸어서 사용중.
**************************************** */

namespace std 
{
	template<class T, class Allocator = std::allocator<T>>
	class list;

	template<class T, class Allocator = std::allocator<T>> 
	class vector;
}

namespace pathfinding
{
	using TileChecker = bool(*)(int x, int y);

	struct Pos
	{
		int	_x;
		int	_y;
	};

	struct Node
	{
		float	_G;
		float	_H;
		float	_F;
		Pos		_pos;
		Node*	_parent;
	};

	/* ***************************************
		노드 간의 비교 함수.
	**************************************** */
	bool CompareNode(Node* node1, Node* node2);

	class BasePathFinding
	{
	public:
		BasePathFinding(float G_weight, float H_weight, int open_list_size, TileChecker tile_check_function);
		~BasePathFinding();
	protected:
		Node*		MakeNode(int x, int y, int dest_x, int dest_y, float G, Node* parent);
		bool		MoveCheck(Node* node);
		bool		PreCreatedChecker(Node* node);
		bool		Insert(Node* node);
		bool		ComparePosWithTile(int x, int y);
		bool		ComparePosWithDest(int x, int y, int dest_x, int dest_y);
		void		FixLineByBresenham();
		TileChecker TileCheck;
		
		float				_G_weight;
		float				_H_weight;
		size_t				_open_list_size;
		std::vector<Node*>*	_open_list;
		std::list<Node*>*	_close_list;
		Node*				_find;
	};

	class JPS : public BasePathFinding
	{
	public:
		JPS(float G_weight, float H_weight, int open_list_size, TileChecker tile_check_function);
		~JPS();
		bool GetNextPath(int* next_x, int* next_y, int start_x, int start_y, int dest_x, int dest_y);
		bool GetAllPath(Pos** path, int start_x, int start_y, int dest_x, int dest_y);

	private:
		bool SearchRR(int x, int y, int dest_x, int dest_y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);
		bool SearchDD(int x, int y, int dest_x, int dest_y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);
		bool SearchLL(int x, int y, int dest_x, int dest_y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);
		bool SearchUU(int x, int y, int dest_x, int dest_y, Node* parent, bool b_only_horizontal_vertical, bool b_create_forced_neighbours);

		void SearchRU(int x, int y, int dest_x, int dest_y, Node* parent);
		void SearchRD(int x, int y, int dest_x, int dest_y, Node* parent);
		void SearchLU(int x, int y, int dest_x, int dest_y, Node* parent);
		void SearchLD(int x, int y, int dest_x, int dest_y, Node* parent);

		void JumpRU(int x, int y, int dest_x, int dest_y, Node* parent);
		void JumpRD(int x, int y, int dest_x, int dest_y, Node* parent);
		void JumpLU(int x, int y, int dest_x, int dest_y, Node* parent);
		void JumpLD(int x, int y, int dest_x, int dest_y, Node* parent);

		bool FindPath(int start_x, int start_y, int dest_x, int dest_y);
	};

	class AStar : public BasePathFinding
	{
	public:
		AStar(float G_weight, float H_weight, int open_list_size, TileChecker tile_check_function);
		~AStar();
		bool GetNextPath(int* next_x, int* next_y, int start_x, int start_y, int dest_x, int dest_y);
		bool GetAllPath(Pos** path, int start_x, int start_y, int dest_x, int dest_y);

	private:
		bool FindPath(int start_x, int start_y, int dest_x, int dest_y);
	};
}