#pragma once

#include <assert.h>

/* ***************************************

[사용 방법]
1. 자료형에 대한 비교 함수 필요.
T로 Node* 와 같은 자료구조가 들어온다면,
그에 맞는 cmp함수를 제공해줘야한다.

ex)
template<>
bool Heap<Node*>::cmp(Node* n1, Node* n2)

2. 첨자 연산 제공.
Heap<자료형> open_list(크기)로 호출.
데이터 보관을 배열을 통하여 했기 때문에
open_list[index].xxx 와 같은 첨자 연산을 제공.

**************************************** */
template <class T>
class Heap
{
public:
	Heap(size_t size)
	{
		_last = 0;
		_capacity = size;
		_arr = new T[size];
	}

	~Heap()
	{
		delete[] _arr;
	}

	bool	push(T data);
	bool	pop();
	T		peek();
	void	clear();
	size_t	size();

	bool	empty()
	{
		return !_last;
	}

	T&	operator[](size_t index)
	{
		return _arr[index];
	}

	bool cmp(T node1, T node2);

private:
	T*		_arr;
	size_t	_last;
	size_t	_capacity;
};

template<class T>
bool Heap<T>::push(T data)
{
	assert(_last < _capacity);

	_arr[_last] = data;

	size_t base_index = _last;
	size_t parent_index = (base_index - 1) / 2;

	while (base_index)
	{
		if (!cmp(_arr[parent_index], _arr[base_index]))
		{
			T data = _arr[parent_index];
			_arr[parent_index] = _arr[base_index];
			_arr[base_index] = data;
		}

		base_index = parent_index;
		parent_index = (base_index - 1) / 2;
	}

	_last++;

	return true;
}

template<class T>
bool Heap<T>::pop()
{
	assert(_last > 0);

	T data = _arr[0];
	_arr[0] = _arr[_last - 1];
	_arr[_last - 1] = data;
	_last--;

	size_t cmp_index = 1;
	size_t base_index = 0;

	while (cmp_index + 1 < _last)
	{
		if (!cmp(_arr[cmp_index], _arr[cmp_index + 1]))
		{
			cmp_index = cmp_index + 1;
		}

		if (!cmp(_arr[base_index], _arr[cmp_index]))
		{
			T data = _arr[base_index];
			_arr[base_index] = _arr[cmp_index];
			_arr[cmp_index] = data;

			base_index = cmp_index;
			cmp_index = cmp_index * 2 + 1;
		}
		else
		{
			cmp_index++;
		}
	}

	return true;
}

template<class T>
T Heap<T>::peek()
{
	return _arr[0];
}

template<class T>
void Heap<T>::clear()
{
	_last = 0;
}

template<class T>
size_t Heap<T>::size()
{
	return _last;
}

template<class T>
bool Heap<T>::cmp(T data1, T data2)
{
	return data1 < data2;
}